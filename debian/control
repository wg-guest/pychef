Source: pychef
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper (>= 10~),
               libssl-dev,
               python (>= 2.6.6-3~),
               python-mock,
               python-setuptools,
               python-unittest2
Standards-Version: 4.0.0
X-Python-Version: >= 2.6
Homepage: https://github.com/coderanger/pychef
Vcs-Git: https://anonscm.debian.org/git/collab-maint/pychef.git
Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/pychef.git
Testsuite: autopkgtest-pkg-python

Package: python-chef
Architecture: all
Depends: libssl-dev,
         python,
         ${misc:Depends},
         ${python:Depends},
         python-setuptools
Description: Python API for interacting with a Chef server
 pychef provides a library with a client and a high-level abstraction
 API to interact with a Chef server.
 .
 Chef is a systems and cloud infrastructure automation framework that
 makes it easy to deploy servers and applications to any physical,
 virtual, or cloud location, no matter the size of the infrastructure. 
 .
 pychef provides all expected features needed to interact with Chef server
 API:
 .
  - Nodes
  - Roles
  - Data bags
  - Environments
  - Search
 .
 pychef additionally provides integration with Fabric (remote deployment tool)
 as an extra feature.
